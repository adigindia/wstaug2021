package day2;

import static org.junit.Assert.*;

import java.util.List;

import org.json.simple.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class StudentGetNamesIDYear {

	public int variableEmpId;
	
	@Before
	public void setUp() throws Exception {

		 RestAssured.baseURI = "http://34.123.48.144:8080";

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		getStudents();
	}
	

	public void getStudents()
	{

		//step 1 - create a request object
		 RequestSpecification request = RestAssured.given();
		 
		 // set any headers or params
		 request.header("Accept", "application/json");

		 // step 3 - call the method get/put/post/delete
		 Response response = request.get("/students/all");
		 
		 // step 4 play with the response
		 int statusCode = response.getStatusCode();
		 System.out.println(statusCode);
		 
		 response.prettyPrint();
		 
		 
		    JsonPath jsonRes = new JsonPath(response.body().asString());
		 
			List<Integer> allIds =jsonRes.get("studentId");
			List<String> allnames =jsonRes.get("firstName");
			List<Integer> allYears =jsonRes.get("dateOfBirth.year");
			
			
			for (int i = 0; i<allIds.size();i++) {
				
				int id = allIds.get(i);
				String name = allnames.get(i);
				int year = allYears.get(i);
				System.out.println("Id = " + id + ": Name = " + name + ": Year =" + year);
				
			}
	}


	
}
