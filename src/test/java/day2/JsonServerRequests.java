package day2;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class JsonServerRequests {

	@Before
	public void setUp() throws Exception {

		 RestAssured.baseURI = "http://localhost:3000";

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {

		getEmployees();

	}
	
	public void getEmployees()
	{

		//step 1 - create a request object
		 RequestSpecification request = RestAssured.given();
		 
		 // set any headers or params
		 request.header("Content-type", "application/json");

		 // step 3 - call the method get/put/post/delete
		 Response response = request.get("/employees");
		 
		 // step 4 play with the response
		 int statusCode = response.getStatusCode();
		 System.out.println(statusCode);
		 
		 response.prettyPrint();

	}

}
