package day2;

import static org.junit.Assert.*;

import java.util.List;

import org.json.simple.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class JsonServerGetAlExtractIdnames {

	public int variableEmpId;
	
	@Before
	public void setUp() throws Exception {

		 RestAssured.baseURI = "http://localhost:3000";

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		getEmployees();
		//createEmployees();
		//deleteEmployees();
	}
	

	public void getEmployees()
	{

		//step 1 - create a request object
		 RequestSpecification request = RestAssured.given();
		 
		 // set any headers or params
		 request.header("Content-type", "application/json");

		 // step 3 - call the method get/put/post/delete
		 Response response = request.get("/employees");
		 
		 // step 4 play with the response
		 int statusCode = response.getStatusCode();
		 System.out.println(statusCode);
		 
		 response.prettyPrint();
		 
		 
		    JsonPath jsonRes = new JsonPath(response.body().asString());
		 
			List<Integer> allIds =jsonRes.get("id");
			List<String> allnames =jsonRes.get("name");
			
			for (int i = 0; i<allIds.size();i++) {
				
				int id = allIds.get(i);
				System.out.println("Id = " + id);
			}

			
			for (int i = 0; i<allnames.size();i++) {
				
				String names = allnames.get(i);
				System.out.println("names = " + names);
			}

	}

	
	public void createEmployees()
	{

		//step 1 - create a request object
		 RequestSpecification request = RestAssured.given();
		 
		 // set any headers or params
		 request.header("Content-type", "application/json");

		 JSONObject requestParams = new JSONObject();

		 requestParams.put("name", "AdityaRA"); // Key Value here
		 requestParams.put("salary", "10000RA"); // Key Value here

		 request.body(requestParams.toJSONString()); // attach json to body

		 // step 3 - call the method get/put/post/delete
		 Response response = request.post("/employees");
		 
		 // step 4 play with the response
		 int statusCode = response.getStatusCode();
		 System.out.println(statusCode);
		 
		 response.prettyPrint();
		 
		 // extract the employee id
		 // save it in a member variable
			JsonPath jsonData = new JsonPath(response.body().asString()); 
			// individual values..
			String name = jsonData.get("name");
			
			System.out.println("Extracted Name = " + name);
			
			variableEmpId= jsonData.get("id");
			
			System.out.println("Extracted id = " + variableEmpId);
			
		

	}

	public void deleteEmployees()
	{
		// consume the member variable

		//step 1 - create a request object
		 RequestSpecification request = RestAssured.given();
		 
		 // set any headers or params
		 request.header("Content-type", "application/json");



		 // step 3 - call the method get/put/post/delete
		 //String id = "112";
		 
		 Response response = request.delete("/employees/"+variableEmpId);
		 
		 // step 4 play with the response
		 int statusCode = response.getStatusCode();
		 System.out.println(statusCode);
		 
		 response.prettyPrint();

	}

	
}
