package day2;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class HelloWorldJunit {

	
	// member variables
	public String name1 = "MemberVariableNOTSET";
	public String salary = "10000";
	
	
	// Prerequests in postman
	@Before
	public void setUp() throws Exception {
		System.out.println("Inside Setup");
	}

	
	// teardowns
	@After
	public void tearDown() throws Exception {
		System.out.println("inside tearDown");
	}

	
	// all tests we will bring it here
	@Test
	public void test() {
		//fail("Not yet implemented");
		System.out.println("inside test");
		
		method1("Aditya");
		method2("Aditya 2");
		method3("Aditya 3");
		System.out.println("inside test Member variable = " + name1);
	
	}

	
	public void method1(String name) {
		System.out.println("Inside method1" + name);
		
		System.out.println("Member variable = " + name1);
		
		name1 = "ADITYA GARG";
	
		System.out.println("Member variable = " + name1);
		
		//local variable
		String variable = "something";

	}
	
	public void method2(String name) {
	
		System.out.println("Inside method2"  + name);
		System.out.println("Member variable = " + name1);
		
		
	}

	public void method3(String name) {
		System.out.println("Inside method3"  + name);
		System.out.println("Member variable = " + name1);
		
	}
	
}
