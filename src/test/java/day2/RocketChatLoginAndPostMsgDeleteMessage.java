package day2;

import static org.junit.Assert.*;

import java.util.List;

import org.json.simple.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class RocketChatLoginAndPostMsgDeleteMessage {


	
	public String userId;
	public String authToken;
	public String messageRoomId;
	public String messageId;
	
	@Before
	public void setUp() throws Exception {

		RestAssured.baseURI = "http://101.53.157.237";

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		
		
		login();
		
		postMessage();
		
		// introduce some delay
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// delete the message
		deleteMessage();
	}


	public void login()
	{

		//step 1 - create a request object
		RequestSpecification request = RestAssured.given();

		// set any headers or params
		request.header("Content-type", "application/json");

		JSONObject requestParams = new JSONObject();

		requestParams.put("user", "mumbai"); // Key Value here
		requestParams.put("password", "test123"); // Key Value here

		request.body(requestParams.toJSONString()); // attach json to body

		// step 3 - call the method get/put/post/delete
		Response response = request.post("/api/v1/login");

		// step 4 play with the response
		int statusCode = response.getStatusCode();
		System.out.println(statusCode);

		response.prettyPrint();
		JsonPath jsonRes = new JsonPath(response.body().asString());

		//extract authtoken and user id
		/*
		 * "data": {
        "userId": "ThHqbB3SMTLd9iLMk",
        "authToken": "I7x72BxrNuvNjifzSbRQUpdvkr9g2spNK69ahJj07RX",
       
		 */
		
		userId = jsonRes.getString("data.userId");
		authToken = jsonRes.getString("data.authToken");
		
		System.out.println("UserID extracted = " +  userId);
		
		System.out.println("authToken extracted = " +  authToken);
		

	}
	
	/*
	 * curl -H "X-Auth-Token: 9HqLlyZOugoStsXCUfD_0YdwnNnunAJF8V47U3QHXSq" \
     -H "X-User-Id: aobEdbYhXfu5hkeqG" \
     -H "Content-type:application/json" \
     http://localhost:3000/api/v1/chat.postMessage \
     -d '{ "channel": "#general", "text": "This is a test!" }'
	 */
	public void postMessage() {
		
		//step 1 - create a request object
		RequestSpecification request = RestAssured.given();

		// set any headers or params
		request.header("Content-type", "application/json");
		request.header("X-Auth-Token", authToken);
		request.header("X-User-Id", userId);

		JSONObject requestParams = new JSONObject();

		requestParams.put("channel", "#ataupdates"); // Key Value here
		requestParams.put("text", "Message from Aditya - Rest Assured"); // Key Value here

		request.body(requestParams.toJSONString()); // attach json to body

		// step 3 - call the method get/put/post/delete
		Response response = request.post("api/v1/chat.postMessage");

		// step 4 play with the response
		int statusCode = response.getStatusCode();
		System.out.println(statusCode);

		response.prettyPrint();
		
		/*
		 * message.rid
message._id
		 */

		JsonPath jsonRes = new JsonPath(response.body().asString());

		
		messageRoomId = jsonRes.getString("message.rid");
		messageId = jsonRes.getString("message._id");
		
		System.out.println("messageRoomId extracted = " +  messageRoomId);
		
		System.out.println("messageId extracted = " +  messageId);

	}
	
	/*
	 * curl -H "X-Auth-Token: 9HqLlyZOugoStsXCUfD_0YdwnNnunAJF8V47U3QHXSq" \
     -H "X-User-Id: aobEdbYhXfu5hkeqG" \
     -H "Content-type:application/json" \
     http://localhost:3000/api/v1/chat.delete \
     -d '{ "roomId": "ByehQjC44FwMeiLbX", "msgId": "7aDSXtjMA3KPLxLjt", "asUser": true }'
	 */
	public void deleteMessage() {
		
		//step 1 - create a request object
		RequestSpecification request = RestAssured.given();

		// set any headers or params
		request.header("Content-type", "application/json");
		request.header("X-Auth-Token", authToken);
		request.header("X-User-Id", userId);

		JSONObject requestParams = new JSONObject();

		requestParams.put("roomId", messageRoomId); // Key Value here
		requestParams.put("msgId",messageId ); // Key Value here
		requestParams.put("asUser",true ); // Key Value here

		request.body(requestParams.toJSONString()); // attach json to body

		// step 3 - call the method get/put/post/delete
		Response response = request.post("/api/v1/chat.delete");

		// step 4 play with the response
		int statusCode = response.getStatusCode();
		System.out.println(statusCode);

		response.prettyPrint();
		

	}



}
