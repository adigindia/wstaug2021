package restassured.reqres;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PostRequestTest {
	
	// @author Aditya Garg
	// https://reqres.in/api
	
	@Test
	public void GetUserDetails()
	{   
		
		RestAssured.baseURI = "https://reqres.in/api/register";
		RequestSpecification httpRequest = RestAssured.given();
		JSONObject requestParams = new JSONObject();
	
		httpRequest.header("Content-Type", "application/json");
		requestParams.put("email", "tracey.ramos@reqres.in"); // Cast
		requestParams.put("password", "pistol");
	
		httpRequest.body(requestParams.toJSONString());
		Response response = httpRequest.post();
	 
		int statusCode = response.getStatusCode();
		//Assert.assertEquals(statusCode, 201);
		System.out.println(statusCode + " :: " + response.body().asString());
		
	}

}