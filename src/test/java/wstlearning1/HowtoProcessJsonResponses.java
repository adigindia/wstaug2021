package wstlearning1;

import java.util.List;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import com.google.gson.Gson;

/*
 * This is implemented using TestNG
 * CP-WST learning series
 * APIEzine
 * Agile Testing Alliance
 * Code example to process JSon Response
 * de-serialization of list of JSON object into POJO (plain old java object)
 * we can use an inner class
 */
public class HowtoProcessJsonResponses {

	
	@Test
	public void f() {
		
		// 164.52.198.132
		RestAssured.baseURI = "http://164.52.198.132:8088"; 

		RequestSpecification req = RestAssured.given();
		req.header("Content-type", "application/json");
		
		Response res = req.get("/api/v2/users");
	    JsonPath jsonRes = new JsonPath(res.body().asString()); 

	    List<Integer> listofUserIds = jsonRes.getList("user.id");
	    
	    System.out.println("size of userid = " + listofUserIds.size());
	    
	    for (int i=0;i<listofUserIds.size();i++) {
	    	
	    	Integer userid = listofUserIds.get(i);
	    	System.out.println(userid.toString());
	    }
	    
	    
	    final List<User> usersList = jsonRes.getList("user", User.class);

	    for (int i=0;i<usersList.size();i++) {
	    	
	    	User  user = usersList.get(i);
	    	user.printall();
	    }

	    res.prettyPrint();
		int statusCode = res.getStatusCode();
		Assert.assertEquals(statusCode, 200);
		
		System.out.println(statusCode + " :: " + res.body().asString());
	}


	// Create an inner class which represents a Json object
	// we can de-serialize a JSON object to a Java object.
	// Gson is required to de-serialize a JSON to Java object
	// 
	public class User {
	
		Integer id;
		String name;
		String surname;
		String adress;
		
	      public void printall() { 
	           System.out.println("inside user class id:" +id.toString() 
	           +
	           "name:" + name +
	           "surname:" + surname +
	           "address:" + adress); 
	      } 
	   } // end of inner class
	
	
}// end of  HowtoProcessJsonResponses class
